/* eslint-disable react/jsx-pascal-case */
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Templates from "./Templates";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Switch>
            <Route exact path={`/`}>
              <Base></Base>
            </Route>
            {Templates.map((Template) => {
              return (
                <Route key={Template} exact path={`${Template.path}`}>
                  <Template.template></Template.template>
                </Route>
              );
            })}
          </Switch>
        </div>
      </Router>
    </div>
  );
}

function Base() {
  return (
    <>
      <ul>
        {Templates.map((el) => {
          return (
            <li key={el}>
              <Link to={`${el.path}`}>{el.name}</Link>
            </li>
          );
        })}
      </ul>
    </>
  );
}

export default App;
