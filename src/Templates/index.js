import Home from './home';
import Seccion from './seccion';

export const Templates = [
    {
        name: "home",
        path: "/home",
        template: Home
    },
    {
        name: "seccion",
        path: "/seccion",
        template: Seccion
    }
]

export default Templates;