import Footer from "./Footer";
import Portada from "./Portada";
import styles from "./styles.module.css";

const Home = () => {
  return (
    <>
      <div className={`${styles["article"]}`}>
        <Portada />
        <Footer />
      </div>
    </>
  );
};

export default Home;
