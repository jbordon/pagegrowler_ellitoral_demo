import styles from "./styles.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faFacebookF,
  faTwitter,
  faWhatsapp,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { faPhoneAlt } from "@fortawesome/free-solid-svg-icons";
import Bell from "../../../assets/images/bell.jpg";

const Footer = () => {
  const suplementsList = [
    {
      id: "1",
      name: "Campolitoral",
      src: "http://www.campolitoral.com.ar/",
    },
    {
      id: "2",
      name: "Revista",
      src: "https://nosotros.ellitoral.com/",
    },
    {
      id: "3",
      name: "Nosotros",
      src: "https://nosotros.ellitoral.com/",
    },
    {
      id: "4",
      name: "Clasificados",
      src: "https://www.ellitoral.com/clasificados/",
    },
    {
      id: "5",
      name: "CYD Litoral",
      src: "https://www.cableydiario.com/",
    },
    {
      id: "6",
      name: "Podcasts",
      src: "https://www.ellitoral.com/index.php/temas/El-Litoral-Podcasts",
    },
    {
      id: "7",
      name: "Mirador Provincial",
      src: "https://www.miradorprovincial.com/",
    },
    {
      id: "8",
      name: "Viví Mejor",
      src: "http://www.vivimejorsf.com.ar/",
    },
    {
      id: "9",
      name: "Puerto Negocios",
      src: "https://puertonegocios.com/",
    },
    {
      id: "10",
      name: "Notife",
      src: "https://notife.com/",
    },
    {
      id: "11",
      name: "Educacion SF",
      src: "https://www.ellitoral.com/index.php/especiales/estudiarensantafe/",
    },
  ];

  const linksList = [
    {
      id: "1",
      name: "Hemeroteca Digital (1930-1979)",
      src: "https://www.santafe.gov.ar/hemerotecadigital/articulo/ellitoral/",
    },
    {
      id: "2",
      name: "Receptorías de avisos",
      src: "https://www.ellitoral.com/index.php/servicios/institucionales/receptorias.php",
    },
    {
      id: "3",
      name: "Administración y Publicidad",
      src: "https://www.ellitoral.com/index.php/servicios/contacto_diario",
    },
    {
      id: "4",
      name: "Elementos institucionales",
      src: "https://www.ellitoral.com/index.php/servicios/institucionales/terceros.php",
    },
    {
      id: "5",
      name: "Opcionales con El Litoral",
      src: "https://www.ellitoral.com/",
    },
    {
      id: "6",
      name: "MediaKit",
      src: "https://www.ellitoral.com/servicios/institucionales/img/logos/mediakit-def.pdf",
    },
  ];

  return (
    <div className={`${styles["footer-container"]}`}>
      <div className={`${styles["footer-logo"]}`}>
        <img src="https://static.ellitoral.com/img/logoellitoral2.svg" alt="" />
      </div>
      <hr />
      <ul className={`${styles["footer-supplements"]}`}>
        {suplementsList.map((el) => (
          <li key={el.id}>
            <div className={`${styles["footer-supplements-item"]}`}>
              <a href={el.src}>{el.name}</a>
            </div>
          </li>
        ))}
      </ul>
      <hr />
      <div className={`${styles["footer-social"]}`}>
        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faFacebookF}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>

        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faTwitter}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>

        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faWhatsapp}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>

        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faYoutube}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>

        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faInstagram}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>

        <div className={`${styles["footer-social-link"]}`}>
          <a href="/">
            <FontAwesomeIcon
              icon={faPhoneAlt}
              className={`${styles["icon"]} `}
            />
          </a>
        </div>
      </div>
      <hr />
      <ul className={`${styles["footer-links"]}`}>
        {linksList.map((el) => (
          <li className={`${styles["footer-links-item"]}`}>
            <a href={el.src}>{el.name}</a>
          </li>
        ))}
      </ul>
      <hr />
      <div className={`${styles["footer-institutional-container"]}`}>
        <img src={Bell} alt="" />

        <div className={`${styles["footer-institutional"]}`}>
          <h5>El Litoral es miembro de:</h5>
          <a href="https://adepa.org.ar/">
            <img
              src="https://www.ellitoral.com/img/58x17xadepa.jpg.pagespeed.ic._UL756M8-x.webp"
              alt=""
            />{" "}
          </a>
          <a href="https://www.iab.com/">
            <img
              src="https://www.ellitoral.com/img/xfooter-iab-uap.jpg.pagespeed.ic.MU1reIZ688.webp"
              alt=""
            />{" "}
          </a>
          <a href="https://www.aadira.org.ar/">
            <img
              src="https://www.ellitoral.com/img/xfooter_medidores.jpg.pagespeed.ic.89C_rgMMhG.webp"
              alt=""
            />
          </a>
        </div>
        <a href="https://serviciosweb.afip.gob.ar/clavefiscal/qr/publicInfoD.aspx">
          <img
            src="https://www.ellitoral.com/image/image/xfooter-data.jpg.pagespeed.ic.C1iUM1BXK8.webp"
            alt=""
          />
        </a>
      </div>
    </div>
  );
};

export default Footer;
