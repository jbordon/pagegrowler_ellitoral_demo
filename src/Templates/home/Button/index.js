import styles from "./styles.module.css";

const Button = ({ textButton }) => {
  return <button className={`${styles["home-button"]}`}>{textButton}</button>;
};

export default Button;
