import React from "react";
import styles from "./styles.module.css";

const TenesQueSaber = () => {
  const tenesQueSaber = [
    {
      id: "1",
      name: "escobar",
      src: "https://static.ellitoral.com/um/fotos/404870_1.jpg",
      text: "Escobar y una jornada de beneficios insuperables",
    },
    {
      id: "2",
      name: "dondevoto",
      src: "https://static.ellitoral.com/um/fotos/404883_20.jpg",
      text: "¿Dónde voto?: ya se puede consultar el padrón provisorio para las PASO",
    },
    {
      id: "3",
      name: "horarios",
      src: "https://static.ellitoral.com/um/fotos/404921_chucji.jpg",
      text: "Horarios y TV: se juega la fecha 13 de la Copa de la Liga Profesional",
    },
    {
      id: "4",
      name: "gente",
      src: "https://static.ellitoral.com/um/fotos/404886_dominguez_predio_kinesiologia.jpg",
      text: "Gente grande y de fiesta clandestina",
    },
  ];
  return (
    <div>
      <h3 className={`${styles["content-right-title"]}`}>TENES QUE SABER</h3>
      <div className={`${styles["content-right-info"]}`}>
        <img
          src="https://media.gq.com.mx/photos/604458fef0cc2a1d8969755c/16:9/w_1280%2cc_limit/10%20autos%20que%20quieren%20ser%20el%20mejor%20de%202021%20-%20BMW%204-Series%202021%20(1).jpg"
          alt=""
        />
        <p>Mayo es Hot Sale en Nation</p>
        <ul className={`${styles["content-right-ul"]}`}>
          <hr />
          {tenesQueSaber.map((el) => (
            <div>
              <li
                key={el.id}
                className={`${styles["content-right-info-item"]}`}
              >
                <div className={`${styles["content-right-container-img"]}`}>
                  <img
                    className={`${styles["content-right-img"]}`}
                    src={el.src}
                    alt=""
                  />
                </div>
                <h5>{el.text}</h5>
              </li>
              <hr className={`${styles["content-right-hr"]}`} />
            </div>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default TenesQueSaber;
