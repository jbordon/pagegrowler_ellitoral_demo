import styles from "./styles.module.css";

const TambienTePuedeInteresar = () => {
  return (
    <div className={`${styles["related-container"]}`}>
      <hr className={`${styles["related-hr"]}`} />
      <div className={`${styles["related-image"]}`}>
        <img
          src="https://static.ellitoral.com/um/fotos/404690_kaf.jpg"
          alt=""
        />
        <div className={`${styles["related-text"]}`}>
          <h4>TE PUEDE INTERESAR</h4>
          <h5 className={`${styles["related-text"]}`}>
            Será conmemorado el día de la patria gallega y de Santiago Apóstol
          </h5>
        </div>
      </div>
      <hr className={`${styles["related-hr"]}`} />
    </div>
  );
};

export default TambienTePuedeInteresar;
