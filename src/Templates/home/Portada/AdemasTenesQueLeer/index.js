import React from "react";
import styles from "./styles.module.css";

const AdemasTenesQueLeer = () => {
  const ademasTenesQueleer = [
    {
      id: "1",
      name: "escobar",
      src: "https://static.ellitoral.com/um/fotos/405864_003_m.jpg",
      text: "La provincia articula con la Nación la asistencia al sector cultural",
    },
    {
      id: "2",
      name: "dondevoto",
      src: "https://static.ellitoral.com/um/fotos/405774_1.jpg",
      text: "La AFIP reglamenta procedimiento para ingresar al régimen de promoción del Norte Grande",
    },
  ];
  return (
    <div className={`${styles["container"]}`}>
      <h3 className={`${styles["content-right-title"]}`}>
        Además tenes que leer:
      </h3>
      <ul className={`${styles["content-right-ul"]}`}>
        <hr className={`${styles["content-right-hr"]}`} />
        {ademasTenesQueleer.map((el) => (
          <div>
            <li key={el.id} className={`${styles["content-right-info-item"]}`}>
              <div className={`${styles["content-right-container-img"]}`}>
                <img
                  className={`${styles["content-right-img"]}`}
                  src={el.src}
                  alt=""
                />
              </div>
              <h5>{el.text}</h5>
            </li>
          </div>
        ))}
        <hr className={`${styles["content-right-hr"]}`} />
      </ul>
    </div>
  );
};

export default AdemasTenesQueLeer;
