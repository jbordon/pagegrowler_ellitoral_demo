import React from "react";
import styles from "./styles.module.css";

const Temas = () => {
  const dataTemas = [
    {
      id: "1",
      url: "/",
      text: "Rosario",
    },
    {
      id: "2",
      url: "/",
      text: "Córdoba",
    },
    {
      id: "3",
      url: "/",
      text: "Entre Ríos",
    },
    {
      id: "4",
      url: "/",
      text: "Corrientes",
    },
    {
      id: "5",
      url: "/",
      text: "La Educación Primero",
    },
    {
      id: "6",
      url: "/",
      text: "Colón",
    },
    {
      id: "7",
      url: "/",
      text: "Clima en Santa Fe",
    },
  ];

  return (
    <div className={`${styles["container"]}`}>
      <hr />
      <div className={`${styles["content-temas"]}`}>
        <h1>#TEMAS:</h1>
        {dataTemas.map((el) => (
          <h3>{el.text}</h3>
        ))}
      </div>
      <hr />
    </div>
  );
};

export default Temas;
