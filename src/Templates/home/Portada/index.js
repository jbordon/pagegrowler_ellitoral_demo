import Button from "../Button";
import data from "../json/data.json";
import AdemasTenesQueLeer from "./AdemasTenesQueLeer";
import LoMasVisto from "./LoMasVisto";
import RedesMin from "./RedesMin";
import Suscribite from "./Suscribite";
import Temas from "./Temas";
import TenesQueSaber from "./TenesQueSaber";
import Comentarios from "./Comentarios";
import TePuedeInteresar from "./TePuedeInteresar";
import SeccionesListado from "./SeccionesListado";
import TambienTePuedeInteresar from "./TambienTePuedeInteresar";
import styles from "./styles.module.css";

const Portada = () => {
  return (
    <>
      <div>
        <div className={`${styles["section-container"]}`}>
          <h3 className={`${styles["section-text"]}`}>sección</h3>
        </div>
        <div className={`${styles["top-container"]}`}>
          <h5>Durante seis meses</h5>
          <h1>{data.item.title}</h1>
          <div dangerouslySetInnerHTML={{ __html: data.item.summary }}></div>
        </div>
        <div className={`${styles["top-image"]}`}>
          <img
            alt={""}
            src={"https://static.ellitoral.com/um/fotos/404662_1.jpg"}
          />
        </div>
        <p className={`${styles["top-epi"]}`}>
          Ante este panorama, se vuelve a afectar la economía de distintos
          sectores como el gastronómico y el hotelero.{" "}
          <em> Foto: Guillermo Di Salvatore </em>
        </p>
        <div className={styles["container"]}>
          <div className={`${styles["main-content"]}`}>
            <div className={`${styles["content-left"]}`}>
              <div>
                <div className={`${styles["article-container"]}`}>
                  <div>
                    <h4 className={`${styles["article-time"]}`}>
                      Jueves 06.05.2021 - Última actualización - <em>7:58</em>
                    </h4>
                    <RedesMin />
                  </div>
                  <div className={`${styles["author-container"]}`}>
                    <img
                      className={`${styles["author-image"]}`}
                      src="https://www.ellitoral.com/um/fotos/autor/x32_balza_n_m.jpg.pagespeed.ic.7mrN09a0rA.webp"
                      alt=""
                    />
                    <div className={`${styles["author-data"]}`}>
                      <h4 className={`${styles["author-text"]}`}>
                        <em>Por: </em>
                        Gabriel Rossini
                      </h4>
                      <Button textButton="seguir" />
                    </div>
                  </div>
                  <p className={`${styles["article-text"]}`}>
                    Con la segunda ola de Covid-19, los gobiernos nacional y
                    provincial implementaron una serie de medidas restrictivas
                    para evitar la circulación de personas y así, intentar
                    frenar la suba de contagios de coronavirus. 
                  </p>
                  <p className={`${styles["article-text"]}`}>
                    Ante este panorama, se vuelve a afectar la economía de
                    distintos sectores com;o el gastronómico y el hotelero. En
                    este marco, en las últimas horas hubo una reunión entre
                    funcionarios, empresarios y gremios para consensuar pasos a
                    seguir y alguna asistencia.
                  </p>

                  <p className={`${styles["article-text"]}`}>
                    Del encuentro se desprende que el gobierno santafesino
                    ayudará al sector hotelero con 10 mil pesos por cada
                    trabajador durante seis meses. El ministro de Trabajo, Juan
                    Manuel Pusineri, encabezó la mesa y expresó: “Continuamos
                    fortaleciendo la mesa de diálogo que hemos conformado desde
                    el año pasado para seguir trabajando con la batería de
                    herramientas que disponemos en lo inmediato”. Gastronómicos
                    Al mismo tiempo, el secretario de Comercio Interior y
                    Servicios, Juan Marcos Aviano, hizo referencia al sector
                    gastronómico. Este sector “como otros rubros de actividad
                    económica del comercio y los servicios afectados por la
                    pandemia, han tenido siempre respuesta de esta gestión de
                    gobierno en lo tarifario, impositivo y tributario. A la par
                    de las medidas nacionales como el ATP, y hoy la
                    disponibilidad de tramitar los REPRO, estamos evaluando las
                    situaciones particulares para avanzar en el sostenimiento de
                    sus empresas, comercios y locales, como así también en la
                    salida futura cuando la segunda ola nos lo permita".
                  </p>
                  <TambienTePuedeInteresar />
                  <h5>Gastronómicos</h5>
                  <p className={`${styles["article-text"]}`}>
                    Del encuentro se desprende que el gobierno santafesino
                    ayudará al sector hotelero con 10 mil pesos por cada
                    trabajador durante seis meses. El ministro de Trabajo, Juan
                    Manuel Pusineri, encabezó la mesa y expresó: “Continuamos
                    fortaleciendo la mesa de diálogo que hemos conformado desde
                    el año pasado para seguir trabajando con la batería de
                    herramientas que disponemos en lo inmediato”. Gastronómicos
                    Al mismo tiempo, el secretario de Comercio Interior y
                    Servicios, Juan Marcos Aviano, hizo referencia al sector
                    gastronómico. Este sector “como otros rubros de actividad
                    económica del comercio y los servicios afectados por la
                    pandemia, han tenido siempre respuesta de esta gestión de
                    gobierno en lo tarifario, impositivo y tributario. A la par
                    de las medidas nacionales como el ATP, y hoy la
                    disponibilidad de tramitar los REPRO, estamos evaluando las
                    situaciones particulares para avanzar en el sostenimiento de
                    sus empresas, comercios y locales, como así también en la
                    salida futura cuando la segunda ola nos lo permita".
                  </p>
                </div>
              </div>
              <div className={`${styles["article-image"]}`}>
                <img
                  src="https://static.ellitoral.com/um/fotos/404714_casa_pa_1.jpg"
                  alt=""
                />
              </div>
              <p className={`${styles["article-epi"]}`}>
                La Ley de Contratos de Trabajo, lo cual requiere de acuerdos en
                los que participa el estado junto con los sindicatos y las
                empresas”.
              </p>
              <hr className={`${styles["article-hr"]}`} />
              <div className={`${styles["article-container"]}`}>
                <h5 className={`${styles["article-intertit-upper"]}`}>
                  FUTURAS MEDIDAS
                </h5>
                <p className={`${styles["article-text"]}`}>
                  Pusineri también aseguró: “Seguiremos utilizando las
                  herramientas con las que ya cuenta el Ministerio de Trabajo,
                  ya sea en el marco de los Procedimientos de Crisis o del Art.
                  223 bis de la Ley de Contratos de Trabajo, lo cual requiere de
                  acuerdos en los que participa el estado junto con los
                  sindicatos y las empresas”.
                </p>
              </div>
              <Temas />
              <RedesMin />
              <AdemasTenesQueLeer />
              <Suscribite />
              <Comentarios />
              <TePuedeInteresar />

              <SeccionesListado />
            </div>
            <div className={`${styles["content-right"]}`}>
              <div className={`${styles["content-right-info"]}`}>
                <img
                  src="https://thumbs.dreamstime.com/z/fondo-gris-132209705.jpg"
                  alt=""
                />
              </div>
              <TenesQueSaber />
              <div className={`${styles["content-right-info"]}`}>
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfYARhyyet4wD9L8QIJqOte_YqIfCLsQJNvNbW0YfckAMAeBpwllWz5GDA9WzP16Nby4Y&usqp=CAU"
                  alt=""
                />
              </div>
              <LoMasVisto />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Portada;
