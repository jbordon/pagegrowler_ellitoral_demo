import styles from "./styles.module.css";

const TePuedeInteresar = () => {
  const dataTePuedeInteresar = [
    {
      id: "1",
      url: "https://static.ellitoral.com/um/fotos/406186_parit.jpg",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "2",
      url: "https://static.ellitoral.com/um/fotos/406177_massa.jpg",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
    {
      id: "3",
      url: "https://static.ellitoral.com/um/fotos/406316_1.jpg",
      text: "La AFIP reglamenta procedimiento para ingresar al régimen de promoción del Norte Grande",
    },
    {
      id: "4",
      url: "https://static.ellitoral.com/um/fotos/406184_sesion.jpg",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "5",
      url: "https://static.ellitoral.com/um/fotos/406191_rave.jpg",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
    {
      id: "6",
      url: "https://static.ellitoral.com/um/fotos/406196_1.jpg",
      text: "La AFIP reglamenta procedimiento para ingresar al régimen de promoción del Norte Grande",
    },
    {
      id: "7",
      url: "https://static.ellitoral.com/um/fotos/406184_sesion.jpg",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "8",
      url: "https://static.ellitoral.com/um/fotos/406186_parit.jpg",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
  ];

  return (
    <div className={`${styles["grid-tpi-container"]}`}>
      <h3>Te puede interesar:</h3>
      <hr />
      <div className={`${styles["grid-tpi"]}`}>
        {dataTePuedeInteresar.map((el) => (
          <div>
            <div key={el.id} className={`${styles["grid-tpi-item"]}`}>
              <div className={`${styles["grid-tpi-container-img"]}`}>
                <img
                  className={`${styles["grid-tpi-img"]}`}
                  src={el.url}
                  alt=""
                />
              </div>
              <p>{el.text}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TePuedeInteresar;
