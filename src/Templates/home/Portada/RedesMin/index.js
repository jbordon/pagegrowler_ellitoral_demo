import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faTwitter,
  faWhatsapp,
} from "@fortawesome/free-brands-svg-icons";

import styles from "./styles.module.css";

const RedesMin = () => {
  return (
    <div className={`${styles["footer-social"]}`}>
      <div className={`${styles["footer-social-link"]}`}>
        <a href="/">
          <FontAwesomeIcon
            icon={faFacebookF}
            className={`${styles["icon"]} `}
          />
        </a>
      </div>

      <div className={`${styles["footer-social-link"]}`}>
        <a href="/">
          <FontAwesomeIcon icon={faTwitter} className={`${styles["icon"]} `} />
        </a>
      </div>

      <div className={`${styles["footer-social-link"]}`}>
        <a href="/">
          <FontAwesomeIcon icon={faWhatsapp} className={`${styles["icon"]} `} />
        </a>
      </div>
    </div>
  );
};

export default RedesMin;
