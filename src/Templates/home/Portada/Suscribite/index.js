import Button from "../../Button";
import styles from "./styles.module.css";

const Suscribite = () => {
  const dataSuscribite = {
    id: "1",
    title: "SUSCRIBITE A NUESTRO NEWSLETTER",
    text: "La provincia articula con la Nación la asistencia al sector cultural",
  };
  return (
    <div className={`${styles["suscribite-container"]}`}>
      <h2>{dataSuscribite.title}</h2>
      <hr />
      <div className={`${styles["suscribite-body"]}`}>
        <Button textButton="SUSCRIBIRME" />
        <p>{dataSuscribite.text}</p>
      </div>
    </div>
  );
};

export default Suscribite;
