import React from "react";
import styles from "./styles.module.css";

const LoMasVisto = () => {
  const dataLoMasVisto = [
    {
      id: "1",
      number: "1",
      text: "La provincia articula con la Nación la asistencia al sector cultural",
    },
    {
      id: "2",
      number: "2",
      text: "La AFIP reglamenta procedimiento para ingresar al régimen de promoción del Norte Grande",
    },
    {
      id: "3",
      number: "3",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "4",
      number: "4",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
    {
      id: "5",
      number: "5",
      text: "Bombas 'molotov' contra un automóvil ",
    },
  ];
  return (
    <div>
      <h3 className={`${styles["content-right-title"]}`}>lo más visto</h3>
      <hr className={`${styles["content-right-hr"]}`} />

      <div className={`${styles["content-right-info"]}`}>
        <img src="https://static.ellitoral.com/um/fotos/405847_1.jpg" alt="" />
        <ul className={`${styles["content-right-ul"]}`}>
          {dataLoMasVisto.map((el) => (
            <div>
              <li
                key={el.id}
                className={`${styles["content-right-info-item"]}`}
              >
                <div className={`${styles["content-right-container-number"]}`}>
                  <h2>{el.number}</h2>
                </div>
                <h5>{el.text}</h5>
              </li>
              <hr className={`${styles["content-right-hr"]}`} />
            </div>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default LoMasVisto;
