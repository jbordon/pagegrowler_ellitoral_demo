import React from "react";
import DejanosTuComentario from "./DejanosTuComentario";
import styles from "./styles.module.css";

const Comentarios = () => {
  const user = {
    id: "1",
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhFOnhYyPucnnU7TR3Xh1G7uq0ZKj7S8DBUA&usqp=CAU",
  };

  const coments = [
    {
      id: "1",
      url: "https://images.pexels.com/photos/931177/pexels-photo-931177.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
      userName: "Amalia Marmol",
      text: "It is a long established fact that a reader will be distracted by the readable content of a page. ",
      time: "1 h",
    },
    {
      id: "2",
      url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQl30opqK-Xw6NSAHpFvuKJEDODt8BqGywObg&usqp=CAU",
      userName: "José Bosch",
      text: "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks.",
      time: "4 h",
    },
    {
      id: "3",
      url: "https://p0.pikrepo.com/preview/881/728/man-standing-beside-tree.jpg",
      userName: "Matías Oscar Castro",
      text: "Richard McClintock, a Latin professor at Hampden-Sydney College, looked up one of the more obscure.",
      time: "6 h",
    },
  ];
  return (
    <div>
      <DejanosTuComentario />
      <form>
        <div className={`${styles["coment-header-container"]}`}>
          <p>{coments.length} comentarios</p>
          <div className={`${styles["coment-header-right"]}`}>
            <p>Ordenar por</p>
            <select name="comentsList" id="">
              <option value="reciente">Lo más reciente</option>
              <option value="antiguo">Lo más antiguo</option>
            </select>
          </div>
        </div>
        <hr className={`${styles["coment-hr"]}`} />
        <div className={`${styles["coment-input-container"]}`}>
          <div className={`${styles["coment-image-container"]}`}>
            <img src={user.url} alt="" />
          </div>
          <input type="text" placeholder="Añade un comentario..." />
        </div>
        <div>
          <ul className={`${styles["coment-ul"]}`}>
            {coments.map((el) => (
              <li key={el.id} className={`${styles["coment-li"]}`}>
                <div className={`${styles["coment-image-container"]}`}>
                  <img src={el.url} alt="" />
                </div>
                <div className={`${styles["coment-data-container"]}`}>
                  <div className={`${styles["coment-text-container"]}`}>
                    <h5>{el.userName}</h5>
                    <p>{el.text}</p>
                  </div>
                  <div
                    className={`${styles["coment-button-actions-container"]}`}
                  >
                    <button>Me gusta -</button>
                    <button>Responder</button>
                    <p>- {el.time}</p>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </form>
    </div>
  );
};

export default Comentarios;
