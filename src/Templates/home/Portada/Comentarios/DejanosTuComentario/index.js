import styles from "./styles.module.css";

const DejanosTuComentario = () => {
  const dataSuscribite = {
    id: "1",
    title: "Dejanos tu comentario",
    text: "Los comentarios realizados son de exclusiva responsabilidad de sus autores y las consecuencias derivadas de ellos pueden ser pasibles de las sanciones legales que correspondan. Evitar comentarios ofensivos o que  no respondan al tema abordado en la información.",
  };
  return (
    <div className={`${styles["dejanos-container"]}`}>
      <h2>{dataSuscribite.title}</h2>
      <hr />
      <div className={`${styles["dejanos-body"]}`}>
        <p>{dataSuscribite.text}</p>
      </div>
    </div>
  );
};

export default DejanosTuComentario;
