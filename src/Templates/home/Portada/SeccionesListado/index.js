import Seccion from "./Seccion";

import styles from "./styles.module.css";

const SeccionesListado = () => {
  return (
    <div className={`${styles["grid-secciones-listado-container"]}`}>
      <Seccion
        titleSecction="Política"
        url="https://static.ellitoral.com/um/fotos/406186_parit.jpg"
      />
      <Seccion
        titleSecction="Área Metropolitana"
        url="https://static.ellitoral.com/um/fotos/406122_spahn.jpg"
      />
      <Seccion
        titleSecction="Sucesos"
        url="https://static.ellitoral.com/um/fotos/406186_parit.jpg"
      />
      <Seccion
        titleSecction="Información general"
        url="https://static.ellitoral.com/um/fotos/406122_spahn.jpg"
      />
    </div>
  );
};

export default SeccionesListado;
