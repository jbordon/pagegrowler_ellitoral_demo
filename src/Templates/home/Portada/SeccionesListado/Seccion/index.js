import styles from "./styles.module.css";

const Seccion = ({ titleSecction, url }) => {
  const dataSeccionesListado = [
    {
      id: "1",
      url: "https://static.ellitoral.com/um/fotos/406186_parit.jpg",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "2",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
    {
      id: "3",
      text: "La AFIP reglamenta procedimiento para ingresar al régimen de promoción del Norte Grande",
    },
    {
      id: "4",
      text: "Córdoba superó los 600.000 vacunados con la primera dosis",
    },
    {
      id: "5",
      text: "Correa, sobre la denuncia por acoso de un DT de AFA: 'Fue en un club hace 7 años'",
    },
  ];
  return (
    <div className={`${styles["secciones-listado-container"]}`}>
      <h3>+{titleSecction}</h3>
      <hr />
      <div className={`${styles["secciones-listado-container-img"]}`}>
        <img
          className={`${styles["secciones-listado-img"]}`}
          src={url}
          alt=""
        />
      </div>
      {dataSeccionesListado.map((el) => (
        <div key={el.id} className={`${styles["secciones-listado-item"]}`}>
          <p>{el.text}</p>
          <hr />
        </div>
      ))}
    </div>
  );
};

export default Seccion;
